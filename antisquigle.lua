started = 0
our_id = 0
chats = {16314802, 36708942, 22632606}
whitelist = {55994550, 77029297, 77026062, 76987175, 82725741, 9427545, 117099167, 103332821}
local inspect = require('inspect')

function inTable(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then return true end
    end
    return false
end

function on_msg_receive (msg)
	if started == 0 then return end
	if msg.out then return end
	if not inTable(chats, msg.to.id) then return end
	if inTable(whitelist, msg.from.id) then return end
	
	-- print(inspect(msg))
	if msg.action and msg.action.type then
		local action = msg.action.type
		-- Check user joins chat
		if action == 'chat_add_user' or action == 'chat_add_user_link' then
			local user
			if msg.action.link_issuer then
				user = msg.from
			else
				user = msg.action.user
			end
			if string.match(user.print_name, "([\216-\219][\128-\191])") then
				send_msg ("chat#17391908", '/kick ' .. user.id .. ' from ' .. msg.to.id, ok_cb, false)
			end
		end
	end
	
	text = msg.from.print_name .. " "
	
	if msg.text then
		text = text .. msg.text
	end
	
	if string.match(text, "([\216-\219][\128-\191])") then
		send_msg ("chat#17391908", '/kick ' .. msg.from.id .. ' from ' .. msg.to.id, ok_cb, false)
	end
end

function on_our_id (id)
  our_id = id
end

function on_binlog_replay_end ()
  started = true
end

function on_secret_chat_update (schat, what) end
function ok_cb(extra, success, result) end
function on_user_update (user, what) end
function on_chat_update (chat, what) end
function on_get_difference_end () end